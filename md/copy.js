document.querySelectorAll('.copy-btn').forEach(button => {
  button.addEventListener('click', () => {
      const codeBlock = button.nextElementSibling;
      const range = document.createRange();
      range.selectNode(codeBlock);
      window.getSelection().removeAllRanges();
      window.getSelection().addRange(range);
      document.execCommand('copy');
      window.getSelection().removeAllRanges();
      // Optional: Show a "Copied!" tooltip or message
      button.textContent = 'Copied!';
      setTimeout(() => {
          button.textContent = 'Copy';
      }, 2000);
  });
});